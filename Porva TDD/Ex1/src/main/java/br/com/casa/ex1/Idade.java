
package br.com.casa.ex1;

import java.util.ArrayList;
import java.util.Scanner;


public class Idade extends Pessoa{

    private static final int PESSOAS = 3;
    private static Scanner input;

    public static void main(String[] args) {
        input = new Scanner(System.in);

        ArrayList<Pessoa> listaDePessoas = new ArrayList<>();
        for(int i = 0; i < PESSOAS; i++){
            Pessoa pessoa = new Pessoa();

            System.out.println("Informe seu nome: ");
                pessoa.setNome(input.nextLine());
            System.out.println("Informe sua Idade: ");
                pessoa.setIdade(input.nextInt());
            input.nextLine();
            listaDePessoas.add(pessoa);
        }

        Pessoa PessoaNova, PessoaVelha;
        PessoaNova = PessoaVelha = listaDePessoas.get(0);

        for(Pessoa pessoa:listaDePessoas){
            if(pessoa.getIdade() > PessoaVelha.getIdade())
                PessoaVelha = pessoa;
            else if(pessoa.getIdade() < PessoaNova.getIdade())
                PessoaNova = pessoa;
        }

        System.out.printf("Pessoa com a idade mais nova é: %s - %d ", PessoaNova.getNome(), PessoaNova.getIdade());
        System.out.printf("Pessoa com a idade mais é velha é: %s - %d ", PessoaVelha.getNome(), PessoaVelha.getIdade());         
    }

    
}